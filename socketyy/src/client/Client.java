package client;

import javafx.event.ActionEvent;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import server.MessageType;

public class Client {
    private DataOutputStream dataOStream;
    private DataInputStream dataIStream;

    public String username, password;
    public Socket socket;

    public boolean isRunning = false;
    private Thread listen;

    private ClientEventLogin eventLogin;
    private ClientEventChat eventChat;

    public Client(String u, String p) {
        username = u;
        password = p;
    }

    public void setEventLogin(ClientEventLogin ev) {
        eventLogin = ev;
    }

    public void setEventChat(ClientEventChat ev) {
        eventChat = ev;
    }

    public void listen() {
        listen = new Thread("Listen") {
            public void run() {
                if (!isRunning) {
                    isRunning = true;
                    sendToAll(
                            "<html><font color=\"orange\" size=\"2\"><b> has joined to conversation.</b></font><br/></html>");

                    while (isRunning) {
                        try {
                            String msg = dataIStream.readUTF();
                            eventChat.messageReceived(msg);
                        } catch (IOException e) {
                            eventChat.disconnectedFromServer();
                            disconnect();
                            isRunning = false;
                        }
                    }
                }
            }
        };
        listen.setDaemon(true);
        listen.start();
    }

    public void sendToAll(String msg) {
        try {
            dataOStream.writeInt(MessageType.MESSAGE);
            dataOStream.writeUTF(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean connect(String ip, int port) {
        boolean result = false;
        try {
            socket = new Socket(ip, port);
            dataOStream = new DataOutputStream(socket.getOutputStream());
            dataIStream = new DataInputStream(socket.getInputStream());
            result = true;
        } catch (IOException e) {
            eventLogin.loginConnectError();
            eventLogin.registerConnectError();
            result = false;
        }
        return result;
    }

    public void disconnect() {
        if (!socket.isClosed()) {
            try {
                dataOStream.writeInt(MessageType.LOGOUT);
                socket.close();
                dataOStream.close();
                dataIStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void login(ActionEvent ev) {
        try {
            dataOStream.writeInt(MessageType.LOGIN);
            dataOStream.writeUTF(username);
            dataOStream.writeUTF(password);
            if (dataIStream.readBoolean()) {
                eventLogin.successfulLogin(ev);
            } else {
                eventLogin.invalidLogin();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void register() {
        try {
            dataOStream.writeInt(MessageType.REGISTER);
            dataOStream.writeUTF(username);
            dataOStream.writeUTF(password);
            if (dataIStream.readBoolean())
                eventLogin.newAccountCreated();
            else
                eventLogin.usernameTaken();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
