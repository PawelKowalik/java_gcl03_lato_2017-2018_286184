package client;

import javafx.event.ActionEvent;

public interface ClientEventLogin {
    void invalidLogin();

    void successfulLogin(ActionEvent event);

    void newAccountCreated();

    void usernameTaken();

    void loginConnectError();

    void registerConnectError();
}
