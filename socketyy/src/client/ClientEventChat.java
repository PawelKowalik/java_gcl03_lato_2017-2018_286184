package client;

public interface ClientEventChat {
    void messageReceived(String msg);

    void disconnectedFromServer();
}
