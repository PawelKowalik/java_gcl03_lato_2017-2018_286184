package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Server {
    private static final int PORT = 2020;
    private List<ClientThread> clients = new ArrayList<>();
    private static Server server;
    private boolean isRunnig = false;
    private ServerSocket finalServerSocket;

    public static void main(String args[]) {
        server = new Server();
        server.start();
    }

    public void start() {
        ServerSocket serverSocket = null;
        final Socket[] socket = new Socket[1];

        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }

        finalServerSocket = serverSocket;

        new Thread("Clients") {
            public void run() {
                if (!isRunnig) {
                    isRunnig = true;
                    while (isRunnig) {
                        try {
                            socket[0] = finalServerSocket.accept();
                        } catch (IOException e) {
                            break;
                        }

                        ClientThread client = new ClientThread(socket[0], server);
                        client.setDaemon(true);
                        client.start();

                        clients.add(client);
                        System.out.println("Client thread added");
                    }
                }
            }
        }.start();
    }

    public void stop() {
        new Thread("Close") {
            public void run() {
                isRunnig = false;

                for (ClientThread client : clients) {
                    client.close();
                    System.out.println("Client thread closed");
                }

                try {
                    finalServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void sayAll(String user, String msg) throws IOException {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss");
        for (ClientThread clientThread : clients)
            clientThread.say("<html><font size=\"2\"><b>[" + dateformat.format(new Date()) + "] " + user
                    + "</b></font></html>" + msg);
    }

    public void removeThread(ClientThread client) {
        clients.remove(client);
    }

}