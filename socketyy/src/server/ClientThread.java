package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Properties;

public class ClientThread extends Thread {
    private String fileName = "config.properties";
    private Properties props = new Properties();
    private Socket socket;
    private Server server;
    private DataInputStream dataIStream = null;
    private DataOutputStream dataOStream = null;
    private String userver;
    public boolean isRunning = false;

    public ClientThread(Socket clientSocket, Server server) {
        this.socket = clientSocket;
        this.server = server;
        try {
            dataIStream = new DataInputStream(socket.getInputStream());
            dataOStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if (!(new File(fileName).isFile()))
            createConfig();
    }

    public void run() {
        if (!isRunning) {
            isRunning = true;
            String line;
            while (isRunning) {
                try {
                    int msgtype = dataIStream.readInt();

                    switch (msgtype) {
                        case MessageType.REGISTER:
                            dataOStream.writeBoolean(register());
                            dataOStream.flush();
                            isRunning = false;
                            break;

                        case MessageType.LOGIN:
                            dataOStream.writeBoolean(login());
                            dataOStream.flush();
                            break;

                        case MessageType.MESSAGE:
                            line = dataIStream.readUTF();
                            server.sayAll(userver, line);
                            break;

                        case MessageType.LOGOUT:
                            try {
                                socket.close();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }

                            server.removeThread(this);

                            if (userver != null) {
                                try {
                                    server.sayAll(userver, " left the conversation.");
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }

                            isRunning = false;
                            break;
                    }
                } catch (IOException e) {
                    System.err.println(userver + " disconnected.");

                    try {
                        socket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    server.removeThread(this);

                    try {
                        server.sayAll(userver, " left the conversation.");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    isRunning = false;
                }
            }
        }
    }

    public void say(String msg) throws IOException {
        dataOStream.writeUTF(msg);
        dataOStream.flush();
    }

    public boolean login() throws IOException {
        boolean result = false;

        String username = dataIStream.readUTF();
        String password = dataIStream.readUTF();

        InputStream input = null;

        try {
            File file = new File(fileName);
            input = new FileInputStream(fileName);

            if (file.isFile()) {
                props.load(input);

                if (props.containsValue(username)) {
                    String filePassword = props.getProperty(username + "password");

                    result = filePassword.equals(password);
                } else
                    result = false;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (result) {
            userver = username;
        }
        return result;
    }

    public boolean register() throws IOException {
        boolean result = false;
        String username = dataIStream.readUTF();
        String pass = dataIStream.readUTF();

        FileWriter output = null;
        try {
            output = new FileWriter(fileName, true);
            props.load(new FileInputStream(fileName));

            if (!props.contains(username)) {
                props.clear();

                props.setProperty(username, username);
                props.setProperty(username + " ,password", pass);

                props.store(output, null);
                result = true;
            } else
                result = false;
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        server.removeThread(this);

        return result;
    }

    private void createConfig() {
        OutputStream output = null;

        try {
            output = new FileOutputStream(fileName);

            String password = "root";

            props.setProperty("root", "root");
            props.setProperty("password", password);
            props.store(output, null);
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void close() {
        try {
            isRunning = false;
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}