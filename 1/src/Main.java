import javax.swing.*;

/**
 * Created by pawel on 06.03.2017.
 */
public class Main extends JFrame
{
    public Main()
    {
        setSize(800,600);
        setTitle("GUI");
        setLayout(null);
        JButton przycisk1 = new JButton("jeden");
        przycisk1.setBounds(600,100,100,50);
        add(przycisk1);

        JButton przycisk2 = new JButton("dwa");
        przycisk2.setBounds(600,170,100,50);
        add(przycisk2);

        JButton przycisk3 = new JButton("trzy");
        przycisk3.setBounds(600,240,100,50);
        add(przycisk3);

        JButton przycisk = new JButton("cztery");
        przycisk.setBounds(600,310,100,50);
        add(przycisk);
    }
    public static void main(String[] args)
    {
        //System.out.println("witaj");
        Main okno=new Main();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);

    }
}
