package example;

public class Program 
{
	private static String addres="students.txt";
	public static void main( String[] args ) throws Exception
	{
		Crawler C=new Crawler(addres);
		C.addIterationStartedListener((iteration)->System.out.println("Iteracja nr "+iteration));
		C.addIterationComplitedListener(iteration->System.out.println("Koniec iteracji\n"));
		C.addStudentaddedListener((Student)->System.out.print("Dodano studenta: "));
		try {
			C.run();
		}
			catch(Exception e) {

			e.printStackTrace();
			System.out.println("ERROR");
		}
	}
}
