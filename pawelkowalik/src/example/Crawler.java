package example;

import java.io.File;
import java.util.*;

public class Crawler {

    public String address=null;
    private List<Student> studentsList;
    private List<Student> secondaryStudentsList;
    private ConsoleLogger consolLoger= new ConsoleLogger();


    public enum OrderMode {
        MARK, FIRST_NAME, LAST_NAME, AGE;
    }

    public enum ExtremumMode {
        MAX, MIN
    }

    public class ConsoleLogger implements Logger {
        @Override
        public void log(String status, Student student) {
            if(student!=null) System.out.println(status + ": " + student.toString());
        }
    }

    Crawler(String address)
    {
        this.address=address;
    }

    private List<IterationListener> iterationStartedListeners = new ArrayList<>();
    public void addIterationStartedListener(IterationListener listener){
        iterationStartedListeners.add(listener);
    }
    public void removeIterationStartedListener(IterationListener listener){iterationStartedListeners.remove(listener); }

    private List<IterationListener> iterationComplitedListeners = new ArrayList<>();
    public void addIterationComplitedListener(IterationListener listener){
        iterationComplitedListeners.add(listener);
    }
    public void removeIterationComplitedListener(IterationListener listener){ iterationComplitedListeners.remove(listener); }

    private List<StudentListener> studentaddedListeners = new ArrayList<>();
    public void addStudentaddedListener(StudentListener listener){
        studentaddedListeners.add(listener);
    }
    public void removeStudentaddedListeners(StudentListener listener){studentaddedListeners.remove(listener);}

    public List<Student> extractStudents(OrderMode mode,List<Student> students)
    {
        switch(mode)
        {
            case AGE:{Collections.sort(students,Student.Comparators.AGE);}break;
            case FIRST_NAME:{Collections.sort(students,Student.Comparators.FIRST_NAME);}break;
            case LAST_NAME:{Collections.sort(students,Student.Comparators.LAST_NAME);}break;
            case MARK:{Collections.sort(students,Student.Comparators.MARK);}break;
            default:{}break;
        }
        return students;
    }

    public double extractMark(ExtremumMode mode,List<Student> students)
    {
        Student result=new Student();
        switch(mode){
            case MAX:{result=Collections.max(students,Student.Comparators.MARK);}break;
            case MIN:{result=Collections.min(students,Student.Comparators.MARK);}break;
            default:{}break;
        }
        return result.getMark();
    }

    public int extractAge(ExtremumMode mode,List<Student> students)
    {
        Student result=new Student();
        switch(mode){
            case MAX:{result=Collections.max(students,Student.Comparators.AGE);}break;
            case MIN:{result=Collections.min(students,Student.Comparators.AGE);}break;
            default:{}break;
        }
        return result.getAge();
    }

    public void run() throws Exception{
        int iteration = 1;
        while(true){

            for(IterationListener el:iterationStartedListeners){
                el.handle(iteration);
            }

            secondaryStudentsList=studentsList;
            studentsList= StudentsParser.parse(new File(address));

            if(studentsList!=null && secondaryStudentsList!=null) {

                boolean flag = false;
                for (Student el : studentsList) {
                    for (Student el2 : secondaryStudentsList) {
                        if (el.equals(el2)) {
                            flag = false;
                            break;
                        } else {
                            flag = true;

                        }
                    }
                    if (flag) {
                        consolLoger.log("ADDED",el);
                    }
                }

                boolean flag2 = false;
                for (Student el : secondaryStudentsList) {
                    for (Student el2 : studentsList) {
                        if (el.equals(el2)) {
                            flag2 = false;
                            break;
                        } else {
                            flag2 = true;
                        }
                    }
                    if (flag2) {
                        consolLoger.log("REMOVED",el);
                    }

                }
            }

            System.out.println("Zakres wieku: "+ extractAge(ExtremumMode.MIN,studentsList)+"-"+extractAge(ExtremumMode.MAX,studentsList));
            System.out.println("Zakres ocen: "+ extractMark(ExtremumMode.MIN,studentsList)+"-"+extractMark(ExtremumMode.MAX,studentsList));
            System.out.println("Posortowane wg ocen: ");
            studentsList = extractStudents(OrderMode.MARK,studentsList);
            for (Student el:studentsList )
            {
                System.out.println(el.getMark() + " " + el.getFirstName() + " " + el.getLastName() + " " + el.getAge());
            }

            Thread.sleep(10000);

            for(IterationListener el:iterationComplitedListeners){
                el.handle(iteration);
            }
            iteration++;
        }



    }

}

