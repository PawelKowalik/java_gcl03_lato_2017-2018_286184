/**
 * Created by Paweł Kowalik on 04.05.2017.
 */
public interface Logger {
    void log(String status, Student student);
}
