/**
 * Created by Paweł Kowalik on 04.05.2017.
 */
public interface Closable {
    void close();
}
