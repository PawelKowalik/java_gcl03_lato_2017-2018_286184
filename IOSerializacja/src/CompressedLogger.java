import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Paweł Kowalik on 04.05.2017.
 */
public class CompressedLogger implements Logger {
    @Override
    public void log(String status, Student student)
    {
        // Konwertowanie daty Java Date do String
        // Pobranie aktualnej daty i godziny
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH.mm.ss.SSS");
        String dateString = dateFormat.format(currentDate);



        StringBuffer content=new StringBuffer("asd");
        createZipFile("archiwum.zip","C:\\Users\\Paweł Kowalik\\Downloads\\IOSerializacja\\src",content);
    }

    public static boolean createZipFile(String fileName, String filePath, StringBuffer content) {
        try {
            File dir = new File(filePath);
            if (!dir.isDirectory()) {
                dir.mkdirs();
            }
            ZipOutputStream z = new ZipOutputStream(new FileOutputStream(new File(dir, fileName + ".zip")));
            z.putNextEntry(new ZipEntry(fileName));
            byte[] bytes = content.toString().getBytes();
            z.write(bytes, 0, bytes.length);
            z.closeEntry();
            z.finish();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
