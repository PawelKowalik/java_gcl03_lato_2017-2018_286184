package Server;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MessageEvent extends  Remote
{
    void messageSended(String message) throws RemoteException;
}
