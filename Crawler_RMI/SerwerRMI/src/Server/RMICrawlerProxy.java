package Server;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RMICrawlerProxy extends UnicastRemoteObject implements CrawlerMethods {

    private Crawler crawler = new Crawler();
    private boolean active = false;
    private MessageEvent event = null;

    protected RMICrawlerProxy() throws RemoteException {

    }


    @Override
    public void start() throws InterruptedException, IOException, CrawlerException {


        this.crawler.sciezka = "plik.txt";
        this.crawler.addStudentaddedListener((Student) -> {
            try {
                prepareMessage("ADDED", Student);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });

        crawler.addStudentremovedListener((Student) -> {
            try {
                prepareMessage("REMOVED", Student);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });


        if (!active)
        {
            crawler.run();
        }
    }

    @Override
    public void setMessageEvent(MessageEvent event) throws RemoteException {
        this.event = event;
    }

    @Override
    public void sendMessages(String txt) throws RemoteException {
            this.event.messageSended(txt);
    }

    public void stop()
    {
        active = false;
    }

    private void prepareMessage(String status, Student student) throws RemoteException {
        sendMessages(status + ";" + student.getMark() + ";" + student.getFirstName() + ";" + student.getLastName() + ";" + student.getAge());
    }

}
