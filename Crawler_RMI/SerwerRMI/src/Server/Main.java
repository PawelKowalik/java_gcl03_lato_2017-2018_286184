package Server;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws InterruptedException, IOException, AlreadyBoundException {

		int port = 5060;

		String name = "rmi://" + port + "/RMICrawlerProxyObject";
		Registry registry = LocateRegistry.createRegistry( port );


		try{
			RMICrawlerProxy crawlerProxy = new RMICrawlerProxy();
			//RMICrawlerProxy stub = (RMICrawlerProxy) UnicastRemoteObject.exportObject(crawlerProxy, 0);
			registry.bind(name, crawlerProxy);

			System.out.println( "Type 'exit' to exit server." );
			Scanner scanner = new Scanner( System.in );

			while ( true )
			{
				if ( scanner.hasNextLine() )
				{
					if ( "exit".equals( scanner.nextLine() ) )
						break;
				}
			}

			scanner.close();
		}
		finally
		{
			UnicastRemoteObject.unexportObject( registry, true ); // zwalnianie rejestru
			System.out.println( "Server stopped." ); // komunikat zakonczenia
		}

	}

}
