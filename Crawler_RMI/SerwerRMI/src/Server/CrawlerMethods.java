package Server;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CrawlerMethods extends Remote{

    public void start() throws InterruptedException, IOException, CrawlerException;
    public void setMessageEvent( MessageEvent event ) throws RemoteException;
    public void sendMessages(String txt) throws RemoteException;
}
