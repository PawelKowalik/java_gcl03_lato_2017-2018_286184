package Client.Controllers;

import Client.Program.Log;
import Client.Program.Statistics;
import Client.Program.StudentOBList;
import Client.Program.StudentProperty;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

public class CrawlerController {

    private  MainController mainController;

    public void setMainController(MainController mainController)
    {
        this.mainController = mainController;
    }

    public CrawlerController(){}


    @FXML
    public void closeAction() {
        Platform.exit();
    }


    @FXML
    public void aboutAction()
    {
        Alert alert = new Alert(Alert.AlertType.NONE);
        alert.setTitle("About");
        alert.getDialogPane().getButtonTypes().add(ButtonType.OK);
        alert.setContentText("Autor: Mateusz Kwarciak");
        alert.show();
    }

    @FXML
    private TableView<StudentProperty> tableView;

    @FXML
    private TableColumn<StudentProperty, Double> markTable;

    @FXML
    private TableColumn<StudentProperty, String> firstNameTable;

    @FXML
    private TableColumn<StudentProperty, String> lastNameTable;

    @FXML
    private TableColumn<StudentProperty, Integer> ageTable;
    /////////////////////////////////////////////////////////////////
    @FXML
    private  TableView<Log> tableLog;

    @FXML
    private TableColumn<Log, String> timeLog;

    @FXML
    private TableColumn<Log, String> statusLog;

    @FXML
    private TableColumn<Log, String> studentDataLog;

    //////////////////////////////////////////////////
    @FXML
    private BarChart<String, Number> barChart;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;


    public final static String[] marksTab = {"2.0", "3.0", "3.5", "4.0", "4.5", "5.0"};
    public Statistics statistics = new Statistics();

    @FXML
    private void initialize()
    {

        ////////////////////////////////////////////////////////////////////////////////////////////////////TABLE VIEW
        markTable.setCellValueFactory(new PropertyValueFactory<StudentProperty, Double>("mark"));
        firstNameTable.setCellValueFactory(new PropertyValueFactory<StudentProperty, String>("firstName"));
        lastNameTable.setCellValueFactory(new PropertyValueFactory<StudentProperty, String>("lastName"));
        ageTable.setCellValueFactory(new PropertyValueFactory<StudentProperty, Integer>("age"));

        tableView.setItems(StudentOBList.studentList);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);


        ////////////////////////////////////////////////////////////////////////////////////////////////////LOG
        timeLog.setCellValueFactory(new PropertyValueFactory<Log, String>("day"));
        statusLog.setCellValueFactory(new PropertyValueFactory<Log, String>("status"));
        studentDataLog.setCellValueFactory(new PropertyValueFactory<Log, String>("studentdata"));

        tableLog.setItems(StudentOBList.logList);
        ////////////////////////////////////////////////////////////////////////////////////////////////////BAR CHART

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Marks");

        series1.getData().add(new XYChart.Data(marksTab[0], 0));
        series1.getData().add(new XYChart.Data(marksTab[1], 0));
        series1.getData().add(new XYChart.Data(marksTab[2], 0));
        series1.getData().add(new XYChart.Data(marksTab[3], 0));
        series1.getData().add(new XYChart.Data(marksTab[4], 0));
        series1.getData().add(new XYChart.Data(marksTab[5], 0));

        Timeline tl = new Timeline();
        tl.getKeyFrames().add(new KeyFrame(javafx.util.Duration.millis(1000),
                actionEvent -> {
                    for (XYChart.Series<String, Number> series : barChart.getData()) {
                        int i=0;
                        for (XYChart.Data<String, Number> data : series.getData()) {
                            data.setYValue(statistics.getStatistics(i));
                            i++;
                        }
                    }
                }));
        tl.setCycleCount(Animation.INDEFINITE);
        tl.play();

        barChart.getData().addAll(series1);
    }


}
