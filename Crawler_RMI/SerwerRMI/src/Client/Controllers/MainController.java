package Client.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class MainController {

    @FXML
    private Group mainPanel;

    @FXML
    public void initialize()
    {
        loadLogPanel();
    }

    public void loadLogPanel()
    {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/Client/FXML/CrawlerScene.fxml"));
        AnchorPane anchorPane = null;
        try{
            anchorPane = loader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
//        LogSceneController logSceneController = loader.getController();
//        logSceneController.setMainController(this);
//        setScene(anchorPane);

        CrawlerController crawlerController = loader.getController();
        crawlerController.setMainController(this);
        setScene(anchorPane);

    }

    public void setScene(AnchorPane anchorPane)
    {
        mainPanel.getChildren().clear();
        mainPanel.getChildren().add(anchorPane);
    }

}
