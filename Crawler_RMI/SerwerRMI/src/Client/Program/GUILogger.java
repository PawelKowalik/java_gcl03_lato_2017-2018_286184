package Client.Program;


import Client.Controllers.CrawlerController;

public class GUILogger implements Logger {

   private CrawlerController crawlerController;
    @Override
    public void log(String status, Student student) {

        StudentProperty sp = new StudentProperty(student.getMark(), student.getFirstName(), student.getLastName(), student.getAge());
        Statistics statistics = new Statistics();

        if("ADDED".equals(status))
        {
            StudentOBList.addStudentOBL(sp, status);
            statistics.updateStatistic(sp,1);
        } else if ("REMOVED".equals(status))
        {
            StudentOBList.removeStudentOBL(sp, status);
            statistics.updateStatistic(sp,-1);
        }

    }
}
