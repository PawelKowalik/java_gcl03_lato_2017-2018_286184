package Client.Program;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StudentOBList {
    public static ObservableList<StudentProperty> studentList =  FXCollections.observableArrayList();
    //private static ListProperty<StudentProperty> listProperty = new SimpleListProperty<>();
    public static ObservableList<Log> logList = FXCollections.observableArrayList();

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //private final static

    public  StudentOBList()
    {
       // listProperty.set(studentList);
    }


    public static void addStudentOBL(StudentProperty student, String status)
    {
        Date date = new Date();
        studentList.add(student);
        studentList.sorted();
        Log log = new Log(dateFormat.format(date), status, student.toString());
        logList.add(log);
    }

    public static void removeStudentOBL(StudentProperty student, String status)
    {
        Date date = new Date();
        StudentProperty tmp=new StudentProperty(0,"","",0);
        for(StudentProperty el: studentList)
        {
            if(el.toString().equals(student.toString()))
                {
                    Log log = new Log(dateFormat.format(date), status, el.toString());
                    logList.add(log);

                    tmp = el;

                    showOBList();
                }
        }
        studentList.remove(tmp);
    }

    public static  void showOBList()
    {

        for (StudentProperty el: studentList)
        {
            System.out.println(el.toString());
        }
    }

}
