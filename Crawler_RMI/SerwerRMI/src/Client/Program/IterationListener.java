package Client.Program;

@FunctionalInterface
public interface IterationListener {
    void handle(int iteration);
}
