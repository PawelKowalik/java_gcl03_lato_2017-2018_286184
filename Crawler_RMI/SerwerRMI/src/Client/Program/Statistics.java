package Client.Program;

public class Statistics {
    private static int[] statistic = new int[6];

    public Statistics()
    {
        for(int el: statistic)
        {
            el=0;
        }
    }

    public void updateStatistic(StudentProperty el, int x)
    {
        if (el.getMarkk() == 2.0)
        {
            statistic[0] +=x;
        }else if (el.getMarkk() ==3.0)
        {
            statistic[1] +=x;
        }else if (el.getMarkk() == 3.5)
        {
            statistic[2] +=x;
        }else if (el.getMarkk() ==4.0)
        {
            statistic[3] +=x;
        }else if (el.getMarkk() ==4.5)
        {
            statistic[4] +=x;
        }else if (el.getMarkk() == 5.0)
        {
            statistic[5] +=x;
        }
    }

    public int getStatistics(int i)
    {
        return  statistic[i];
    }
}
