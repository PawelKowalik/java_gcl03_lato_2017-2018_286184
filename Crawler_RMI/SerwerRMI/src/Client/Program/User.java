package Client.Program;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class User {
    private StringProperty loginProperty = new SimpleStringProperty();
    private StringProperty passwordProperty = new SimpleStringProperty();
    private IntegerProperty ageProperty = new SimpleIntegerProperty();
    private StringProperty addressProperty = new SimpleStringProperty();
    private StringProperty sexProperty = new SimpleStringProperty();

    public User(String login, String password, int age, String address, String sex)
    {
        loginProperty.set(login);
        passwordProperty.set(password);
        ageProperty.set(age);
        addressProperty.set(address);
        sexProperty.set(sex);
    }

    public StringProperty getLoginProperty(){return  loginProperty;}
    public StringProperty getPasswordProperty(){return  passwordProperty;}
    public IntegerProperty getAgeProperty(){return  ageProperty;}
    public StringProperty getAddressProperty(){return  addressProperty;}
    public StringProperty getSexProperty(){return  sexProperty;}

    public String getStringLoginProperty(){return  loginProperty.get();}
    public String getStringPasswordProperty(){return  passwordProperty.get();}

    public void setLoginProperty(StringProperty loginProperty){
        this.loginProperty = loginProperty;
    }

    public void setPasswordProperty(StringProperty passwordProperty){
        this.passwordProperty = passwordProperty;
    }

    public void setAgeProperty(IntegerProperty ageProperty){
        this.ageProperty = ageProperty;
    }

    public void setAddressProperty(StringProperty addressProperty){
        this.addressProperty = addressProperty;
    }

    public void setSexProperty(StringProperty sexProperty){
        this.sexProperty = sexProperty;
    }


    public  String toString()
    {
        return loginProperty.get() + " " + passwordProperty.get();
    }
}

