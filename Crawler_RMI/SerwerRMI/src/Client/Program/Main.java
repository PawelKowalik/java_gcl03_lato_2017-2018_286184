package Client.Program;

import Server.*;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Main {


    public static void main(String[] args) throws InterruptedException, IOException, Server.CrawlerException, NotBoundException {

       GUIThread guiThread = new GUIThread();
       Thread thread = new Thread(guiThread);
       thread.start();

        String host = "localhost";
        int port = 5060;

        String name = "rmi://" + port + "/RMICrawlerProxyObject";

        Registry registry = LocateRegistry.getRegistry(host, port);
        CrawlerMethods crawlerMethods = (CrawlerMethods) registry.lookup(name);


        crawlerMethods.setMessageEvent(new CustomMessageEvent());
        crawlerMethods.start();



    }
    private static class CustomMessageEvent extends UnicastRemoteObject implements MessageEvent
    {
        private static final long serialVersionUID = 6738492455439057283L;


        protected CustomMessageEvent() throws RemoteException
        {
            super();
        }

        @Override
        public void messageSended( String message )
        {
            String[] parts = message.split(";");
            String status = parts[0];
            Student student = new Student();
            student.setMark(Double.parseDouble(parts[1]));
            student.setFirstName(parts[2]);
            student.setLastName(parts[3]);
            student.setAge(Integer.parseInt(parts[4]));

            GUILogger guiLogger = new GUILogger();
            guiLogger.log(status,student);
            System.out.println( message );
        }

    }
}
