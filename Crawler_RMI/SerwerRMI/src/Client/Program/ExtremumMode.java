package Client.Program;

import java.util.List;

public class ExtremumMode {

    public static double minMark(List<Student> lista)
    {
        double wynik =6.0;
        for (Student el: lista)
        {
            if (wynik  > el.getMark())
            {
                wynik=el.getMark();
            }
        }
        return  wynik;
    }

    public static double maxMark(List<Student> lista)
    {
        double wynik =0;
        for (Student el: lista)
        {
            if (wynik  < el.getMark())
            {
                wynik=el.getMark();
            }
        }
        return  wynik;
    }

    public static double minAge(List<Student> lista)
    {
        double wynik =200;
        for (Student el: lista)
        {
            if (wynik  > el.getAge())
            {
                wynik=el.getAge();
            }
        }
        return  wynik;
    }

    public static double maxAge(List<Student> lista)
    {
        double wynik =0;
        for (Student el: lista)
        {
            if (wynik  < el.getAge())
            {
                wynik=el.getAge();
            }
        }
        return  wynik;
    }
}
