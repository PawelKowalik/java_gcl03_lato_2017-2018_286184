package Client.Program;

@FunctionalInterface
public interface StudentListener {
    void handle(Student student);
}
