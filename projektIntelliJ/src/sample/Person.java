package sample;

import javafx.beans.property.*;

/**
 * Created by Paweł Kowalik on 09.04.2017.
 */
public class Person {
    private final DoubleProperty mark = new SimpleDoubleProperty();
    private final StringProperty firstName = new SimpleStringProperty();
    private final StringProperty lastName = new SimpleStringProperty();
    private final IntegerProperty age = new SimpleIntegerProperty();

    public Person() {
        this(0,"","",0);
    }

    public Person(double ma, String fn, String ln, int ag) {
        mark.set(ma);
        firstName.set(fn);
        lastName.set(ln);
        age.set(ag);
    }

    public double getMark() {
        return mark.get();
    }

    public DoubleProperty markProperty() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark.set(mark);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public int getAge() {
        return age.get();
    }

    public IntegerProperty ageProperty() {
        return age;
    }

    public void setAge(int age) {
        this.age.set(age);
    }
}

