package sample;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {
    @FXML
    Button logInButton,signInButton,clearAll,save,cancel,back;

    @FXML
    TextField logsignin,password,age,adress,loginP,passwordP;

    @FXML
    CheckBox female,male;

    @FXML
    Menu aboutMenu;

//    @FXML
//    TableView<Person> tableView;

  //  @FXML
    //public void initialize(){

    //}

    @FXML
    public void loginPanelControl(ActionEvent event) throws IOException{
        Stage stage;
        Parent root;
        if (event.getSource()==signInButton){
            stage=(Stage) signInButton.getScene().getWindow();
            root= FXMLLoader.load(getClass().getResource("signIn.fxml"));

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
        else if((event.getSource()==logInButton)&&(loginP.getText().equals("123"))&&(passwordP.getText().equals("123"))){
            stage=(Stage) logInButton.getScene().getWindow();
            root= FXMLLoader.load(getClass().getResource("crawler.fxml"));

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }


    }

    @FXML
    private void closeClicked(){
        System.exit(1);
    }

   /* @FXML
    public void aboutPopUpWindow (ActionEvent event) throws IOException{
        Stage stage;
        Parent root;
        if(event.getSource()==aboutMenu){
            stage=new Stage();
            root=FXMLLoader.load(getClass().getResource("about.fxml"));
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(aboutMenu.getScene().getWindow());
            stage.showAndWait();
        }
       // else {

        //}
    }*/

   @FXML
   public void signInPanel (ActionEvent event) throws IOException{
       Stage stage;
       Parent root;
       if(event.getSource()==save||event.getSource()==cancel||event.getSource()==back){
           stage=(Stage) save.getScene().getWindow();
           root= FXMLLoader.load(getClass().getResource("logIn.fxml"));

           Scene scene = new Scene(root);
           stage.setScene(scene);
           stage.show();
       }
       else if(event.getSource()==clearAll){
           logsignin.clear();
           password.clear();
           age.clear();
           adress.clear();
           female.setSelected(false);
           male.setSelected(false);
       }
   }

   @FXML
    public void aboutClick(){
       Alert alert=new Alert(Alert.AlertType.INFORMATION,"JA ZEM TO UCZYNIL",ButtonType.OK);
       alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
       alert.setTitle("TITLEABOUT");
       alert.setContentText("MAIN");
       alert.showAndWait();
   }


}
