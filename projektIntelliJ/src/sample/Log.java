package sample;

import javafx.beans.property.*;

/**
 * Created by Paweł Kowalik on 11.04.2017.
 */
public class Log {

        private final StringProperty day = new SimpleStringProperty();
        private final StringProperty status = new SimpleStringProperty();
        private final StringProperty studentData = new SimpleStringProperty();

        public Log() {
            this("","","");
        }

        public Log(String d, String s, String sd) {
            day.set(d);
            status.set(s);
            studentData.set(sd);
        }

    public String getDay() {
        return day.get();
    }

    public StringProperty dayProperty() {
        return day;
    }

    public void setDay(String day) {
        this.day.set(day);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getStudentData() {
        return studentData.get();
    }

    public StringProperty studentDataProperty() {
        return studentData;
    }

    public void setStudentData(String studentData) {
        this.studentData.set(studentData);
    }
}
