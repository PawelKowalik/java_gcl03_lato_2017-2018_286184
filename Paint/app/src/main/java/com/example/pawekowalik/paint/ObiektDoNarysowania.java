package com.example.pawekowalik.paint;

/**
 * Created by Paweł Kowalik on 02.06.2017.
 */

import android.graphics.RectF;

public class ObiektDoNarysowania {

    public int kolor;
    public RectF figura;

    public ObiektDoNarysowania(int kolor, RectF figura) {
        this.kolor = kolor;
        this.figura = figura;
    }

}