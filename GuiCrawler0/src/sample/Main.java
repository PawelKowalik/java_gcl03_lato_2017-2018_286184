package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.effect.Glow;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Crawler 5.0");

        //MENU GÓRNE
        MenuBar menuBar = new MenuBar();
        Menu menuFile = new Menu();
        menuFile=CustomMenuBar.menuFile();
        Menu menuAbout = new Menu();
        menuAbout=CustomMenuBar.menuAbout();

        menuBar.getMenus().addAll(menuFile, menuAbout);

        //MENU GÓRNE KONIEC

        //ZAKŁADKI

        HBox hBox=new HBox();
        //hBox.setPadding(new Insets(10));
        hBox.setSpacing(3);

        TabPane tabPane = new TabPane();
        tabPane=CustomTabPane.tabPane();
        CustomTabPane.tabPane();

        hBox.getChildren().addAll(tabPane);

        //ZAKŁADKI KONIEC

        //TABELA

        //tableView<TableModel> tabela;

        //TABELA KONIEC

        Scene scene = new Scene(new VBox(), 800, 500);
        ((VBox) scene.getRoot()).getChildren().addAll(menuBar,hBox);
        //primaryStage.setOpacity(0.95);
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.UTILITY);//styl stage'a
        primaryStage.setX(-2);//miejsce uruchomienia stage'a X
        primaryStage.setY(0);//miejsce uruchomienia stage'a Y
        primaryStage.setResizable(false);//zablokowanie skalowania okna
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
