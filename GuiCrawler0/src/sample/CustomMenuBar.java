package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;

import java.awt.*;

/**
 * Created by Paweł Kowalik on 26.03.2017.
 */
public class CustomMenuBar extends MenuBar {
    public static Menu menuFile() {
        // --- Menu File
        Menu menuFile = new Menu("Program");
        MenuItem exit = new MenuItem("Exit");
        exit.setAccelerator(KeyCombination.keyCombination("Ctrl+C"));
        exit.setOnAction((ActionEvent t) -> {
            System.exit(0);
        });
        menuFile.getItems().addAll(exit);
        return menuFile;
    }
    public static Menu menuAbout() {
        // --- Menu About
        Label menuLabel = new Label("About");
        menuLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DialogAlert.dialogAle();
            }
        });

        Menu menuAbout = new Menu();
        menuAbout.setGraphic(menuLabel);

        return menuAbout;
    }
}
