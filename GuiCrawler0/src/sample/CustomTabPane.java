package sample;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

/**
 * Created by Paweł Kowalik on 26.03.2017.
 */
public class CustomTabPane extends AnchorPane{
    public  static TabPane tabPane(){
        TabPane tabPane = new TabPane();

        Tab students = new Tab();
        students.setText("Students");
        Tab log = new Tab();
        log.setText("Log");
        Tab histogram = new Tab();
        histogram.setText("Histogram");

        tabPane.getTabs().addAll(students, log, histogram);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        return tabPane;
    }
}


