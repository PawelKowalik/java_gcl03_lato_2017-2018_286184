package sample;

import javafx.scene.control.Alert;

/**
 * Created by Paweł Kowalik on 25.03.2017.
 */
public class DialogAlert {
    public static void dialogAle(){
        Alert infomationAlert=new Alert(Alert.AlertType.INFORMATION);
        infomationAlert.setTitle("OKNO INFORMACJI");
        infomationAlert.setHeaderText("O aplikacji...");
        infomationAlert.setContentText("Aby kontynuować naciśnij OK");
        infomationAlert.showAndWait();
    }
}
